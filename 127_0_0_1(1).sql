-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27 Mar 2018 pada 06.26
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smk7smg_portal`
--
CREATE DATABASE IF NOT EXISTS `smk7smg_portal` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `smk7smg_portal`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id_menu` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `submenu_included` tinyint(1) NOT NULL,
  `multi_post` tinyint(1) NOT NULL,
  `priority_involved` tinyint(1) NOT NULL,
  `external_reference` tinyint(1) NOT NULL,
  `external_site` text,
  `last_update_timestamp` timestamp NULL DEFAULT NULL,
  `last_update_user` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu`
--

INSERT INTO `tb_menu` (`id_menu`, `name`, `submenu_included`, `multi_post`, `priority_involved`, `external_reference`, `external_site`, `last_update_timestamp`, `last_update_user`, `create_timestamp`, `create_user`) VALUES
(1, 'Profil', 1, 0, 0, 0, NULL, '2018-03-26 03:00:21', 17, '2018-02-08 18:53:36', 17),
(2, 'PPDB', 0, 0, 0, 1, 'http://jateng.siap-ppdb.com', NULL, NULL, '2018-03-19 02:25:46', 17);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu_history`
--

CREATE TABLE `tb_menu_history` (
  `id_menu_history` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `submenu_included` tinyint(1) NOT NULL DEFAULT '0',
  `multi_post` tinyint(1) NOT NULL DEFAULT '0',
  `priority_involved` tinyint(1) NOT NULL DEFAULT '0',
  `external_reference` tinyint(1) NOT NULL DEFAULT '0',
  `external_site` text,
  `type` varchar(20) NOT NULL,
  `update_timestamp` timestamp NULL DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `delete_timestamp` timestamp NULL DEFAULT NULL,
  `delete_user` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NULL DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu_history`
--

INSERT INTO `tb_menu_history` (`id_menu_history`, `id_menu`, `name`, `submenu_included`, `multi_post`, `priority_involved`, `external_reference`, `external_site`, `type`, `update_timestamp`, `update_user`, `delete_timestamp`, `delete_user`, `create_timestamp`, `create_user`) VALUES
(1, 2, 'PPDB', 0, 0, 0, 1, 'http://jateng.siap-ppdb.com', 'CREATE', NULL, NULL, NULL, NULL, '2018-03-19 02:25:46', 17),
(2, 3, 'Artikel', 0, 1, 0, 0, NULL, 'CREATE', NULL, NULL, NULL, NULL, '2018-03-21 07:58:26', 17),
(7, 1, 'Profil', 1, 0, 1, 0, NULL, 'UPDATE', '2018-03-26 03:00:21', 17, NULL, NULL, NULL, NULL),
(8, 3, 'Artikel', 0, 1, 0, 0, NULL, 'DELETE', NULL, NULL, '2018-03-26 03:25:29', 17, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu_sub1`
--

CREATE TABLE `tb_menu_sub1` (
  `id_menu_sub1` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `multi_post` tinyint(1) NOT NULL,
  `priority_involved` tinyint(1) NOT NULL,
  `external_reference` tinyint(1) NOT NULL,
  `external_site` text,
  `last_update_timestamp` timestamp NULL DEFAULT NULL,
  `last_update_user` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu_sub1`
--

INSERT INTO `tb_menu_sub1` (`id_menu_sub1`, `id_menu`, `name`, `multi_post`, `priority_involved`, `external_reference`, `external_site`, `last_update_timestamp`, `last_update_user`, `create_timestamp`, `create_user`) VALUES
(2, 1, 'Sejarah', 0, 0, 0, NULL, NULL, NULL, '2018-03-23 12:46:12', 17);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu_sub1_history`
--

CREATE TABLE `tb_menu_sub1_history` (
  `id_menu_sub1_history` int(11) NOT NULL,
  `id_menu_sub1` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `multi_post` tinyint(4) NOT NULL DEFAULT '0',
  `priority_involved` tinyint(4) NOT NULL DEFAULT '0',
  `external_reference` tinyint(4) NOT NULL DEFAULT '0',
  `external_site` text,
  `type` varchar(20) NOT NULL,
  `update_timestamp` timestamp NULL DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `delete_timestamp` timestamp NULL DEFAULT NULL,
  `delete_user` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NULL DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu_sub1_history`
--

INSERT INTO `tb_menu_sub1_history` (`id_menu_sub1_history`, `id_menu_sub1`, `id_menu`, `name`, `multi_post`, `priority_involved`, `external_reference`, `external_site`, `type`, `update_timestamp`, `update_user`, `delete_timestamp`, `delete_user`, `create_timestamp`, `create_user`) VALUES
(1, 2, 1, 'Sejarah', 0, 0, 0, NULL, 'CREATE', NULL, NULL, NULL, NULL, '2018-03-23 12:46:12', 17),
(2, 3, 1, 'Visi dan Misi', 0, 0, 0, NULL, 'CREATE', NULL, NULL, NULL, NULL, '2018-03-23 12:58:10', 17),
(9, 3, 1, 'Visi dan Misi', 0, 0, 0, NULL, 'DELETE', NULL, NULL, '2018-03-25 09:27:31', 17, NULL, NULL),
(10, 4, 1, 'Sambutan Kepala Sekolah', 0, 0, 1, 'https://smkn7semarang.blogspot.com/', 'CREATE', NULL, NULL, NULL, NULL, NULL, 17),
(11, 4, 1, 'Sambutan Kepsek', 0, 0, 1, 'https://smkn7semarang.blogspot.com/', 'UPDATE', NULL, 17, NULL, NULL, NULL, NULL),
(12, 4, 1, 'Sambutan Kepsek', 0, 0, 1, 'https://smkn7semarang.blogspot.co.id/p/profil.html', 'UPDATE', NULL, 17, NULL, NULL, NULL, NULL),
(13, 4, 1, 'Sambutan Kepsek', 0, 0, 1, 'https://smkn7semarang.blogspot.co.id/p/profil.html', 'DELETE', NULL, NULL, '2018-03-25 23:53:40', 17, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_post`
--

CREATE TABLE `tb_post` (
  `id_post` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_menu_sub1` int(11) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `preview` text,
  `preview_image` text,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `last_revise_timestamp` timestamp NULL DEFAULT NULL,
  `last_revise_user` int(11) DEFAULT NULL,
  `last_update_timestamp` timestamp NULL DEFAULT NULL,
  `last_update_user` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_post`
--

INSERT INTO `tb_post` (`id_post`, `id_menu`, `id_menu_sub1`, `title`, `content`, `preview`, `preview_image`, `approved`, `last_revise_timestamp`, `last_revise_user`, `last_update_timestamp`, `last_update_user`, `create_timestamp`, `create_user`) VALUES
(1, 1, 2, 'Sejarah SMK Negeri 7 Semarang', 'Pada zaman dahulu kala, terdapat satu sekolah yang bernama STM Pembangunan Semarang.\r\nTamat.\r\n\r\n', 'Heehehehheheheh, jadi cuma gini ? seru juga', 'http://localhost/sekolah/assets/img/tumpeng.JPG', 3, NULL, NULL, NULL, NULL, '2018-02-27 22:47:37', 17);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_post_history`
--

CREATE TABLE `tb_post_history` (
  `id_post_history` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_menu_sub1` int(11) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `preview` text,
  `preview_image` text,
  `approved` tinyint(4) NOT NULL,
  `revise_timestamp` timestamp NULL DEFAULT NULL,
  `revise_user` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `update_timestamp` timestamp NULL DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `delete_timestamp` timestamp NULL DEFAULT NULL,
  `delete_user` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NULL DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_post_history`
--

INSERT INTO `tb_post_history` (`id_post_history`, `id_post`, `id_menu`, `id_menu_sub1`, `title`, `content`, `preview`, `preview_image`, `approved`, `revise_timestamp`, `revise_user`, `type`, `update_timestamp`, `update_user`, `delete_timestamp`, `delete_user`, `create_timestamp`, `create_user`) VALUES
(17, 2, 2, 3, 'Kejuaran Futsal', 'rangers stemba atau futsal wanita di STM Pembangunan semarnag meraih juara 1 tingka nasional, dengan kaptennya bernama Faqih Fauzia S\r\n\r\n', 'rangers stemba berhasil merebut gelarnya kembali', NULL, 2, NULL, NULL, 'RELATION DELETE', NULL, NULL, '2018-03-25 09:27:31', 17, NULL, NULL),
(18, 3, 3, 3, 'Harga Lombok Naik', 'Para penjual makanan di kantin berhenti berjual nasi goreng\r\n', 'Para penjual makanan di kantin berhenti berjual nasi goreng', NULL, 2, NULL, NULL, 'RELATION DELETE', NULL, NULL, '2018-03-25 09:27:31', 17, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_pass` text NOT NULL,
  `user_fullname` varchar(200) NOT NULL,
  `user_uniqueid` varchar(50) NOT NULL,
  `user_level` int(11) NOT NULL DEFAULT '0',
  `last_update_timestamp` timestamp NULL DEFAULT NULL,
  `last_update_user` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `user_name`, `user_pass`, `user_fullname`, `user_uniqueid`, `user_level`, `last_update_timestamp`, `last_update_user`, `create_timestamp`, `create_user`) VALUES
(7, 'brianidon', '25f9e794323b453885f5181f1b624d0b', 'BRIAN JAYA', '123456789', 2, '2018-03-07 16:37:18', NULL, '2018-02-20 21:34:31', 0),
(10, 'leo_messi', '25d55ad283aa400af464c76d713c07ad', 'LEONEL MESSI', '121212121212121', 0, '2018-02-22 12:15:49', NULL, '2018-02-22 12:15:49', 0),
(12, 'taktuntwang@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 'TAKTUNTWANG', '1200000000000', 0, '2018-03-04 19:07:51', NULL, '2018-03-04 19:06:12', 0),
(15, 'ayu_rak', '25f9e794323b453885f5181f1b624d0b', 'AYU RAKHMADANI', '13131313131313', 2, '2018-03-06 18:40:10', NULL, '2018-03-06 18:39:16', 0),
(16, 'brian@apa.com', '329b299fe6b6f1a8547772811da919fe', 'brianMarcius', '', 1, NULL, NULL, '2018-03-12 21:08:44', 0),
(17, 'msandyka', 'AEA7FDF2B9DC4F2765F70431FFF5A095', 'Milton Sandyka', '1413605', 1, NULL, NULL, '2018-03-14 19:12:58', 17);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user_access`
--

CREATE TABLE `tb_user_access` (
  `id_user_access` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_menu_sub1` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user_access_history`
--

CREATE TABLE `tb_user_access_history` (
  `id_user_access_history` int(11) NOT NULL,
  `id_user_access` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_menu_sub1` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `delete_timestamp` timestamp NULL DEFAULT NULL,
  `delete_user` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NULL DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user_history`
--

CREATE TABLE `tb_user_history` (
  `id_user_history` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_pass` text NOT NULL,
  `user_fullname` varchar(200) NOT NULL,
  `user_uniqueid` varchar(50) NOT NULL,
  `user_level` int(11) NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL,
  `update_timestamp` timestamp NULL DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `delete_timestamp` timestamp NULL DEFAULT NULL,
  `delete_user` int(11) DEFAULT NULL,
  `create_timestamp` timestamp NULL DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user_history`
--

INSERT INTO `tb_user_history` (`id_user_history`, `id_user`, `user_name`, `user_pass`, `user_fullname`, `user_uniqueid`, `user_level`, `type`, `update_timestamp`, `update_user`, `delete_timestamp`, `delete_user`, `create_timestamp`, `create_user`) VALUES
(4, 15, 'ayu_rak', '25f9e794323b453885f5181f1b624d0b', 'AYU RAKHMADANI', '121212121212121212121212', 1, 'CREATE', NULL, NULL, NULL, NULL, '2018-03-06 18:39:17', NULL),
(5, 15, 'ayu_rak', '25f9e794323b453885f5181f1b624d0b', 'AYU RAKHMADANI', '13131313131313', 2, 'UPDATE', '2018-03-06 18:40:10', NULL, NULL, NULL, '2018-03-07 00:40:12', NULL),
(6, 11, 'mo', '912ec803b2ce49e4a541068d495ab570', 'lemon', '131313', 0, 'DELETE', NULL, NULL, '2018-03-07 13:47:24', NULL, '2018-03-07 19:47:26', NULL),
(7, 7, 'brianidon', '25f9e794323b453885f5181f1b624d0b', 'BRIAN JAYA', '123456789', 2, 'UPDATE', '2018-03-07 16:37:18', NULL, NULL, NULL, '2018-03-07 22:37:20', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `tb_menu_history`
--
ALTER TABLE `tb_menu_history`
  ADD PRIMARY KEY (`id_menu_history`);

--
-- Indexes for table `tb_menu_sub1`
--
ALTER TABLE `tb_menu_sub1`
  ADD PRIMARY KEY (`id_menu_sub1`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indexes for table `tb_menu_sub1_history`
--
ALTER TABLE `tb_menu_sub1_history`
  ADD PRIMARY KEY (`id_menu_sub1_history`);

--
-- Indexes for table `tb_post`
--
ALTER TABLE `tb_post`
  ADD PRIMARY KEY (`id_post`),
  ADD KEY `id_menu` (`id_menu`),
  ADD KEY `id_menu_sub1` (`id_menu_sub1`);

--
-- Indexes for table `tb_post_history`
--
ALTER TABLE `tb_post_history`
  ADD PRIMARY KEY (`id_post_history`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`,`user_name`),
  ADD UNIQUE KEY `user_uniqueid` (`user_uniqueid`);

--
-- Indexes for table `tb_user_access`
--
ALTER TABLE `tb_user_access`
  ADD PRIMARY KEY (`id_user_access`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_menu` (`id_menu`),
  ADD KEY `id_menu_sub1` (`id_menu_sub1`);

--
-- Indexes for table `tb_user_access_history`
--
ALTER TABLE `tb_user_access_history`
  ADD PRIMARY KEY (`id_user_access_history`);

--
-- Indexes for table `tb_user_history`
--
ALTER TABLE `tb_user_history`
  ADD PRIMARY KEY (`id_user_history`,`id_user`,`user_name`,`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_menu_history`
--
ALTER TABLE `tb_menu_history`
  MODIFY `id_menu_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_menu_sub1`
--
ALTER TABLE `tb_menu_sub1`
  MODIFY `id_menu_sub1` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_menu_sub1_history`
--
ALTER TABLE `tb_menu_sub1_history`
  MODIFY `id_menu_sub1_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_post`
--
ALTER TABLE `tb_post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_post_history`
--
ALTER TABLE `tb_post_history`
  MODIFY `id_post_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tb_user_access`
--
ALTER TABLE `tb_user_access`
  MODIFY `id_user_access` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user_access_history`
--
ALTER TABLE `tb_user_access_history`
  MODIFY `id_user_access_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user_history`
--
ALTER TABLE `tb_user_history`
  MODIFY `id_user_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
