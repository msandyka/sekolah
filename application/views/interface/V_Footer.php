 <footer class="py-5 bg-primary" style="padding-bottom: 20px !important">
      <div class="container">
        <div class="row">
          <div class="col-md-4" style="color:white;opacity: 0.5">
            <img style="width: 60%;" src="<?php echo base_url('assets/img/logo.png')?>"><br>
           <br><h4>Smk Negeri 7 Semarang</h4>
           <p>STM Pembangunan Semarang</p>
          </div>
          <div class="col-md-4" style="color:white;opacity: 0.5;padding-left: 5%">
            <h2>Contact Us</h2><br>
             
              <i class="fa fa-phone">&nbsp;024 8447649</i><br><br>
              <i class="fa fa-envelope">&nbsp;smkn7semarang@yahoo.co.id</i><br><br>
              <i class="fa fa-map-marker">&nbsp;Jalan Simpang Lima Semarang</i><br>
             
          </div>          
          <div class="col-md-4" style="color:white;"">
            <h2 style="opacity: 0.5">Leave a Comment</h2><br>
            <input class="form-control" type="text" name="usename" placeholder="Username"><br>
            <textarea class="form-control" placeholder="Leave a Comment"></textarea><br>
            <button class="btn btn-default">Submit</button>
          </div>                    
        </div>
        <!-- <p class="m-0 text-center" style="color: white;opacity: 0.5">Copyright &copy; Your Website 2018</p> -->
      </div>
      <!-- /.container --><br>
    <p class="m-0 text-center" style="color: white;opacity: 0.5;">Copyright &copy; SMK Negeri 7 Semarang 2018</p>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript">var base_url = <?php echo base_url(); ?></script>
    <script src="<?php echo base_url('assets/temp/AdminLTE-2.4.2/bower_components/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-4.0.0-dist/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/front_end.js');?>"></script>
 
 <script>
// Get the elements with class="column"
// var elements = document.getElementsByClassName("column");

// // Declare a loop variable
// var i;

// // Full-width images
// function one() {
//     for (i = 0; i < elements.length; i++) {
//         elements[i].style.msFlex = "100%";  // IE10
//         elements[i].style.flex = "100%";
//     }
// }

// // Two images side by side
// function two() {
//     for (i = 0; i < elements.length; i++) {
//         elements[i].style.msFlex = "50%";  // IE10
//         elements[i].style.flex = "50%";
//     }
// }

// Four images side by side
// function four() {
//     for (i = 0; i < elements.length; i++) {
//         elements[i].style.msFlex = "25%";  // IE10
//         elements[i].style.flex = "25%";
//     }
// }

// // Add active class to the current button (highlight it)
// var header = document.getElementById("myHeader");
// var btns = header.getElementsByClassName("btn-gal");
// for (var i = 0; i < btns.length; i++) {
//   btns[i].addEventListener("click", function() {
//     var current = document.getElementsByClassName("active-gal");
//     current[0].className = current[0].className.replace("active-gal", "");
//     this.className += " active-gal";
//   });
// }
</script>
<script>
// Get the modal
// var modal = document.getElementById('myModal');

// // Get the image and insert it inside the modal - use its "alt" text as a caption
// var img = document.getElementById('myImg');
// var modalImg = document.getElementById("img01");
// var captionText = document.getElementById("caption");
// img.onclick = function(){
//     modal.style.display = "block";
//     modalImg.src = this.src;
//     captionText.innerHTML = this.alt;
// }

// // Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close")[0];

// // When the user clicks on <span> (x), close the modal
// span.onclick = function() { 
//     modal.style.display = "none";
// }

// $('.gal-gal').each(function(){
//   $(this).click(function(){
//     // $('#myModal').modal('hide');
//     var src=$(this).attr('src');
//     alert(src);
//     $('#img01').attr('src',src);
//     // $('#myModal').modal('show');
//   })
// })

$('.gal-gal').click(function(){
  var src = $(this).attr('src');
   $('#img01').attr('src',src);

})
</script>

  </body>

</html>