 <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <?php 
        if($multiPost!=1)
        {
          foreach ($getMenuInfo as $menu)
          {
      ?>
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $menu['name'];?></h3>
          <a style="float:right" alt="Insert Menu" title="Insert Menu" href="<?php echo base_url('Admin/PostManagement_editSingleMenu/'.$id_menu.'/'.$id_menu_sub1.'/');?>">
            <button type="button" class="btn btn-warning" >
              <i class="fa fa-pencil-square-o fa-2x"></i>
            </button>
          </a>
        </div>
        <!-- /.box-header -->
        <?php
            foreach ($getPost as $post)
            {
        ?>
        <!-- <div class="box-body">
          <div class="row">
            <div class="col-lg-12">
              <h3><?php echo $post['title'];?></h3>
            </div>
            <div class="col-lg-12">
              <p><h4>by <?php echo $post['user_fullname'];?></h4></p>
              <hr/>
              <p>
                <h5>
                  Edisi 
                  <?php
                    if(empty($post['last_update_timestamp']))
                    {
                      echo $post['create_timestamp'];
                    }
                    else
                    {
                      if(strtotime($post['last_update_timestamp'])>strtotime($post['create_timestamp']))
                      {
                        echo date('Y-m-d H:i:s', strtotime($post['last_update_timestamp']));
                      }
                      else
                      {
                        echo date('Y-m-d H:i:s', strtotime($post['create_timestamp']));
                      }
                    }
                  ?>
                </h5>
              </p>
            </div>
            <div class="col-lg-12">
              <?php
                $preview_image  =   '#';
                if(empty($post['preview_image']))
                {
                  $preview_image  = base_url('assets/img/SMKN7.jpg');
                }
                else
                {
                  $preview_image  = $post['preview_image'];
              ?>
              <?php
                }
              ?>
              <img src="<?php echo $preview_image;?>" class="img-responsive img-rounded center-block" width="375">  
            </div>
            <div class="col-lg-12">
              <p>
                <?php echo $post['preview'];?>
              </p>
            </div>
          </div>
        </div> -->
        <div class="box-body">
          <form class="form-horizontal" method="post" action="<?php echo base_url('Admin/MenuManagement_insertMenu/');?>" enctype="multipart/form-data">
            <div class="form-group">
              <label for="PostManagement-editSingleMenu-txtPostTitle" class="control-label col-lg-2">Title</label>
              <div class="col-lg-6">
                <input type="text" class="form-control" name="txtPostTitle" id="PostManagement-editSingleMenu-txtPostTitle" value="<?php echo $post['title'];?>" >  
              </div>
            </div>
            <div class="form-group">
              <label for="PostManagement-editSingleMenu-txtPostPreviewImage" class="control-label col-lg-2">Preview Image</label>
              <div class="col-lg-6">
                <input type="file" name="txtPostPreviewImage" id="PostManagement-editSingleMenu-txtPostPreviewImage">
              </div>
            </div>
            <div class="form-group">
              <label for="PostManagement-editSingleMenu-txtPostPreview" class="control-label col-lg-2">Preview</label>
              <div class="col-lg-10">
                <textarea name="txtPostPreview" id="PostManagement-editSingleMenu-txtPostPreview" rows="4"><?php echo $post['preview'];?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="PostManagement-editSingleMenu-txtPostPreview" class="control-label col-lg-2">Content</label>
              <div class="col-lg-10">
                <textarea name="txtPostPreview" id="PostManagement-editSingleMenu-txtPostPreview" rows="4"><?php echo $post['content'];?></textarea>
              </div>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <?php
            }
          }
        }
      ?>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>