 <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <?php 
            if($multiPost!=1)
            {
              foreach ($getMenuInfo as $menu)
              {
          ?>
          <div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $menu['name'];?></h3>
              <a style="float:right" alt="Insert Menu" title="Insert Menu" href="<?php echo base_url('Admin/PostManagement_editSingleMenu/'.$id_menu.'/'.$id_menu_sub1.'/');?>">
                <button type="button" class="btn btn-warning" >
                  <i class="fa fa-pencil-square-o fa-2x"></i>
                </button>
              </a>
            </div>
            <!-- /.box-header -->
            <?php
                foreach ($getPost as $post)
                {
            ?>
            <div class="box-body">
              <div class="row">
                <div class="col-lg-12">
                  <h3><?php echo $post['title'];?></h3>
                </div>
                <div class="col-lg-12">
                  <p><h4>by <?php echo $post['user_fullname'];?></h4></p>
                  <hr/>
                  <p>
                    <h5>
                      Edisi 
                      <?php
                        if(empty($post['last_update_timestamp']))
                        {
                          echo $post['create_timestamp'];
                        }
                        else
                        {
                          if(strtotime($post['last_update_timestamp'])>strtotime($post['create_timestamp']))
                          {
                            echo date('Y-m-d H:i:s', strtotime($post['last_update_timestamp']));
                          }
                          else
                          {
                            echo date('Y-m-d H:i:s', strtotime($post['create_timestamp']));
                          }
                        }
                      ?>
                    </h5>
                  </p>
                </div>
                <div class="col-lg-12">
                  <?php
                    $preview_image  =   '#';
                    if(empty($post['preview_image']))
                    {
                      $preview_image  = base_url('assets/img/SMKN7.jpg');
                    }
                    else
                    {
                      $preview_image  = $post['preview_image'];
                  ?>
                  <?php
                    }
                  ?>
                  <img src="<?php echo $preview_image;?>" class="img-responsive img-rounded center-block" width="375">  
                </div>
                <div class="col-lg-12">
                  <p>
                    <?php echo $post['preview'];?>
                  </p>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <?php
                }
              }
            }
          ?>
          <!-- <div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Artikel</h3>
              <a style="float:right" alt="Insert Menu" title="Insert Menu" >
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#MenuManagement-insertMenu">
                  <i class="fa fa-plus fa-2x"></i>
                </button>
              </a>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-hover" id="MenuManagement-menuList">
                <thead>
                  <tr>
                    <th style="vertical-align: middle; text-align: center;">No.</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    
                  </tr>
                </tbody>
              </table>
            </div>
          </div> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>